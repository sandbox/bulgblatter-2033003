<?php
function commerceecont_settings_form($form, $form_state) {
  $store_address = variable_get('commerceecont_store_address', array());
  /* Credentials for e-Econt */
  $form['wrapper'] = array(
      '#type' => 'fieldset',
      '#title' => t('Econt credentials'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );
  $form['wrapper']['username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#required' => TRUE,
      '#default_value' => variable_get('commerceecont_user', 'demo'),
  );
  $form['wrapper']['password'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#required' => TRUE,
      '#default_value' => variable_get('commerceecont_pass', 'demo'),
  );
  $form['wrapper']['mode'] = array(
      '#type' => 'radios',
      '#options' => array('live' => t("Live"), 'demo' => t('Demo')),
      '#title' => t('Service usage mode'),
      '#default_value' => variable_get('commerceecont_mode', 'demo'),
  );
  $options = array();
  if (variable_get('commerceecont_log_request', FALSE)) {
    $options[] = 'request';
  }
  if (variable_get('commerceecont_log_response', FALSE)) {
    $options[] = 'response';
  }
  if (variable_get('commerceecont_save_response', FALSE)) {
    $options[] = 'save_response';
  }
  $form['wrapper']['commerceecont_log'] = array(
      '#type' => 'checkboxes',
      '#options' => array(
          'request' => t('Save API request messages in watchdog'),
          'response' => t('Save API response messages  in watchdog'),
          'save_response' => t('Save response as file (public://econt/shipping)'),
      ),
      '#title' => t('Log the following messages for debugging (do not use in production)'),
      '#default_value' => $options,
  );

  /* STORE ADDRESS - Ship from address */
  $form['store'] = array(
      '#type' => 'fieldset',
      '#title' => t('Ship from address'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );
  $form['store']['organisation_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Company'),
//      '#required' => TRUE,
      '#default_value' => isset($store_address['organisation_name']) ? $store_address['organisation_name'] : ''
  );
  $form['store']['name_line'] = array(
      '#type' => 'textfield',
      '#title' => t('Contact name'),
      '#required' => TRUE,
      '#default_value' => isset($store_address['name_line']) ? $store_address['name_line'] : ''
  );
  $form['store']['city'] = array(
      '#type' => 'textfield',
      '#title' => t('City'),
      '#required' => TRUE,
      '#autocomplete_path' => 'commerceecont/autocomplete/city',
      '#default_value' => isset($store_address['city']) ? $store_address['city'] : ''
  );
  $form['store']['post_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Post code'),
      '#required' => TRUE,
      '#autocomplete_path' => 'commerceecont/autocomplete/post_code',
      '#default_value' => isset($store_address['post_code']) ? $store_address['post_code'] : ''
  );
  $form['store']['quarter'] = array(
      '#type' => 'textfield',
      '#title' => t('Quarter'),
      '#autocomplete_path' => 'commerceecont/autocomplete/quarter',
      '#states' => array(
          'required' => array(
              'input[name="street"]' => array('value' => '')
          )
      ),
      '#default_value' => isset($store_address['quarter']) ? $store_address['quarter'] : ''
  );
  $form['store']['street'] = array(
      '#type' => 'textfield',
      '#title' => t('Street'),
      '#autocomplete_path' => 'commerceecont/autocomplete/street',
      '#states' => array(),
      '#states' => array(
          'required' => array(
              'input[name="quarter"]' => array('value' => '')
          )
      ),
      '#default_value' => isset($store_address['street']) ? $store_address['street'] : ''
  );
  $form['store']['street_num'] = array(
      '#type' => 'textfield',
      '#title' => t('Street N'),
      '#size' => 4,
      '#states' => array(
          'optional' => array(
              'input[name="street"]' => array('value' => '')
          )
      ),
      '#default_value' => isset($store_address['street_num']) ? $store_address['street_num'] : ''
  );
  $form['store']['block'] = array(
      '#type' => 'textfield',
      '#title' => t('Block N'),
      '#size' => 4,
      '#default_value' => isset($store_address['block']) ? $store_address['block'] : ''
  );
  $form['store']['other'] = array(
      '#type' => 'textfield',
      '#title' => t('Address extra details (entrance, floor, apartment etc)'),
      '#default_value' => isset($store_address['other']) ? $store_address['other'] : ''
  );
  $form['store']['phone'] = array(
      '#type' => 'textfield',
      '#title' => t('Phone'),
      '#required' => TRUE,
      '#default_value' => isset($store_address['phone']) ? $store_address['phone'] : ''
  );
  $form['store']['mail'] = array(
      '#type' => 'textfield',
      '#title' => t('Email for notifications'),
      '#required' => TRUE,
      '#default_value' => isset($store_address['mail']) ? $store_address['mail'] : variable_get('site_mail', '')
  );

  /* Available services */
  $form['services'] = array(
      '#type' => 'fieldset',
      '#title' => t('Shipping service settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );
  $delivery_from = variable_get('commerceecont_deliver_from', 'OFFICE');
  $form['services']['commerceecont_deliver_from'] = array(
      '#title' => t('Select delivery source'),
      '#type' => 'radios',
      '#options' => array(
          'OFFICE' => t('Delivery from Econt\'s office'),
          'DOOR' => t('Delivery from door'),
      ),
      '#default_value' => $delivery_from,
      '#ajax' => array(
          'callback' => '_commerceecont_delivery_callback',
          'wrapper' => 'service-wrapper',
          'method' => 'replace',
          'effect' => 'fade',
      ),
  );
  $options = array();
  $officess = commerceecont_offices(array('postal_code' => $store_address['post_code']));
  foreach ($officess as $office) {
    $options[$office->office_code] = $office->name . ': ' . $office->address;
  }
  $form['services']['commerceecont_deliver_from_office'] = array(
    '#title' => t('Select Econt office'),
    '#type' => 'select',
    '#states' => array(
        'visible' => array(
            ':input[name="commerceecont_deliver_from"]' => array('value' => 'OFFICE'),
        ),
    ),
    '#options' => $options,
    '#default_value' => variable_get('commerceecont_deliver_from_office', ''),
  );
  $form['services']['swrapper'] = array(
      '#type' => 'container',
      '#attributes' => array(
          'id' => 'service-wrapper'
      )
  );
  if (isset($form_state['values'])) {
    $delivery_from = $form_state['values']['commerceecont_deliver_from'];
  }
  $options = array();
  foreach (_commerceecont_service_list($delivery_from) as $key => $service) {
    $options[$key] = $service['title'];
  }
  $form['services']['swrapper']['commerceecont_services'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Select Available services'),
      '#options' => $options,
      '#default_value' => variable_get('commerceecont_services', array()),
  );
  $options = array();
  foreach (_commerceecont_service_list($delivery_from) as $key => $service) {
    $options[$service['slug']] = $service['title'];
  }
  $form['services']['swrapper']['commerceecont_default_service'] = array(
      '#type' => 'radios',
      '#title' => t('Select Default service'),
      '#options' => $options,
      '#default_value' => variable_get('commerceecont_default_service', 'office_office'),
  );
  $form['services']['swrapper']['commerceecont_discount'] = array(
    '#type' => 'textfield',
    '#title' => t('Discount %'),
    '#description' => t('Only numbers'),
    '#default_value' => variable_get('commerceecont_discount', 0),
  );

  /* default package */
//  $form['wrapper_package'] = array(
//      '#type' => 'fieldset',
//      '#title' => t('Default package settings'),
//      '#collapsible' => TRUE,
//      '#collapsed' => TRUE,
//  );

  /* Update info */
  $address_db_errors = commerceecont_check_address_info(FALSE, TRUE);
  $form['wrapper_u'] = array(
      '#type' => 'fieldset',
      '#title' => t('Refresh Econt address database'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('Econt uses its own address database, which is used to validate "from" and "to" destinations. The module
      needs this data in order to generate acceptable request to API. You can try to download data from the server.
      Data to download and parse is about 26MB. Success depends on your server settings. On failure you can use default
      database provided with module.')
  );
  $db_stats = '';
  foreach ($address_db_errors as $e) {
    $db_stats['items'][] = t('Table %table contains <strong>%rows rows</strong>', array('%table' => $e['table'], '%rows' => $e['count']));
  }
  $form['wrapper_u']['reset'] = array(
      '#type' => 'checkbox',
      '#title' => t('Refresh info from Econt server'),
      '#description' => t('This will clear local address database and will try to download it
    from Econt server. Could take some time or not succeed depends on yor server settings.')
  );
  $form['wrapper_u']['update_info'] = array(
      '#type' => 'submit',
      '#value' => t('Update Econt database from server'),
      '#limit_validation_errors' => array(),
      '#description' => t('Update from server or cached data from the last request'),
      '#submit' => array('commerceecont_settings_form_submit')
);

  $form['wrapper_u']['update_info_default'] = array(
      '#type' => 'submit',
      '#value' => t('Restore Econt database from defaults'),
      '#limit_validation_errors' => array(),
      '#description' => t('Load address database from files provided with this module'),
      '#submit' => array('commerceecont_settings_form_submit')
  );
  $form['wrapper_u']['stats'] = array(
      '#markup' => theme('item_list', $db_stats),
  );
  $form['save_setting'] = array(
      '#type' => 'submit',
      '#value' => t('Save settings')
  );

  return $form;
}

function commerceecont_settings_form_validate($form, &$form_state) {
  global $language;
  if ($form_state['values']['op'] == t('Save settings')) {
    $field_name = $language->language == 'bg' ? 'name' : 'name_en';
    $query = db_select('commerceecont_cities', 'cc')
        ->fields('cc', array('id'))
        ->condition('cc.' . $field_name, $form_state['values']['city']);
    $result = $query->execute()->fetchField();
    if (empty($result)) {
      form_set_error('city', t('Invalid city'));
    }
    if (!empty($form_state['values']['post_code'])) {
      $query->condition('cc.post_code', $form_state['values']['post_code']);
      $result = $query->execute()->fetchField();
      if (empty($result)) {
        form_set_error('post_code', t('Invalid post code'));
      }
    }
    if (!empty($form_state['values']['street'])) {
      $query->join('commerceecont_streets', 'cs', 'cc.id = cs.id_city');
      $query->condition('cs.' . $field_name, $form_state['values']['street']);
      $result = $query->execute()->fetchField();
      if (empty($result)) {
        form_set_error('street', t('Invalid street name'));
      }
    }
    if (!empty($form_state['values']['quarter'])) {
      $query->join('commerceecont_quarters', 'cq', 'cc.id = cq.id_city');
      $query->condition('cq.' . $field_name, $form_state['values']['quarter']);
      $result = $query->execute()->fetchField();
      if (empty($result)) {
        form_set_error('quarter', t('Invalid quarter'));
      }
    }

    if (!valid_email_address($form_state['values']['mail'])) {
      form_set_error('mail', t('Invalid email address'));
    }
    if (!in_array(drupal_strtoupper($form_state['values']['commerceecont_default_service']), $form_state['values']['commerceecont_services'], TRUE)) {
      form_set_error('commerceecont_default_service', t('Default service should be Available!'));
    }
  }
}

function commerceecont_settings_form_submit($form, &$form_state) {
  if ($form_state['values']['op'] == t('Save settings')) {
    $store_address = array(
        'organisation_name' => $form_state['values']['organisation_name'],
        'name_line' => $form_state['values']['name_line'],
        'city' => $form_state['values']['city'],
        'post_code' => $form_state['values']['post_code'],
        'street' => $form_state['values']['street'],
        'street_num' => $form_state['values']['street_num'],
        'quarter' => $form_state['values']['quarter'],
        'block' => $form_state['values']['block'],
        'other' => $form_state['values']['other'],
        'phone' => $form_state['values']['phone'],
        'mail' => $form_state['values']['mail'],
    );
    $services = variable_get('commerceecont_store_address', array());
    variable_set('commerceecont_store_address', $store_address);
    variable_set('commerceecont_services', $form_state['values']['commerceecont_services']);
    variable_set('commerceecont_default_service', $form_state['values']['commerceecont_default_service']);
    variable_set('commerceecont_mode', $form_state['values']['mode']);
    variable_set('commerceecont_user', $form_state['values']['username']);
    variable_set('commerceecont_pass', $form_state['values']['password']);
    variable_set('commerceecont_log_request', $form_state['values']['commerceecont_log']['request'] === 'request');
    variable_set('commerceecont_log_response', $form_state['values']['commerceecont_log']['response'] === 'response');
    variable_set('commerceecont_save_response', $form_state['values']['commerceecont_log']['save_response'] === 'save_response');
    variable_set('commerceecont_deliver_from', $form_state['values']['commerceecont_deliver_from']);
    variable_set('commerceecont_deliver_from_office', $form_state['values']['commerceecont_deliver_from_office']);
    variable_set('commerceecont_discount', $form_state['values']['commerceecont_discount']);
    // If the selected services have changed then rebuild caches.
    if ($services !== $form_state['values']['commerceecont_services']) {
      commerce_shipping_services_reset();
      entity_defaults_rebuild();
      rules_clear_cache(TRUE);
      menu_rebuild();
    }
  } elseif ($form_state['values']['op'] == t('Update Econt database from server')) {
    $reset = isset($form_state['values']['reset']) && $form_state['values']['reset'] == 1 ? TRUE : FALSE;
    commerceecont_init_info_update($reset);
  } elseif ($form_state['values']['op'] == t('Restore Econt database from defaults')) {
    commerceecont_import_address_database();
  }
}

/**
 * Generate Econt info update batch
 */
function commerceecont_init_info_update($reset = FALSE) {
  $types = commerceecont_admin_types_array();
  $operations = array();
  foreach ($types as $type) {
    $type['reset'] = $reset;
    $operations[] = array(
        'commerceecont_update_info',
        array(
            $type,
            t('(Processing @operation)', array('@operation' => $type['type'])),
        ),
    );
  }
  $batch = array(
      'operations' => $operations,
      'finished' => 'commerceecont_batch_finished',
  );
  batch_set($batch);
}

function commerceecont_admin_types_array() {
  return array(
      'cities' => array(
          'type' => 'cities',
          'xmltype' => 'cities',
          'table_name' => 'commerceecont_cities',
          'filename' => 'public://econt/info/econt_cities.xml',
      ),
      'cities_quarters' => array(
          'type' => 'cities_quarters',
          'xmltype' => 'cities_quarters',
          'table_name' => 'commerceecont_quarters',
          'filename' => 'public://econt/info/econt_quarters.xml'
      ),
      'cities_regions' => array(
          'type' => 'cities_regions',
          'xmltype' => 'cities_regions',
          'table_name' => 'commerceecont_regions',
          'filename' => 'public://econt/info/econt_cities_regions.xml'
      ),
      'cities_streets' => array(
          'type' => 'cities_streets',
          'xmltype' => 'cities_street',
          'table_name' => 'commerceecont_streets',
          'filename' => 'public://econt/info/econt_cities_streets.xml'
      ),
      'offices' => array(
          'type' => 'offices',
          'xmltype' => 'offices',
          'table_name' => 'commerceecont_offices',
          'filename' => 'public://econt/info/econt_offices.xml'
      ),
  );
}



/*
 * function commerceecont_admin_update_cities($xml_array) {
  $xml = '<?xml version="1.0" ?>' . format_xml_elements($xml_array);
//  $xml_response = commerceecont_api_request($xml);
  $xml_response = file_get_contents(drupal_get_path('module', 'commerceecont').'/econt_cities.xml');
  if (!$xml_response) return FALSE;
  $xml_element = new SimpleXMLElement($xml_response);
  if (!$xml_element->cities) return FALSE;
  $values = array();
  foreach ($xml_element->cities->e as $value) {
    $values[] = array(
        'id' => (string) $value->id,
        'post_code' => (string)$value->post_code,
        'type' => (string)$value->type,
        'id_zone' => (string)$value->id_zone,
        'name' => (string)$value->name,
        'name_en' => (string)$value->name_en,
        'region' => (string)$value->region,
        'region_en' => (string)$value->region_en,
        'id_country' => (string)$value->id_country,
        'id_office' => (string)$value->id_office,
        'updated_time' => strtotime((string)$value->updated_time),
    );
  }
  db_query('TRUNCATE commerceecont_cities');
  $query = db_insert('commerceecont_cities')->fields(array('id', 'post_code', 'type', 'id_zone', 'name', 'name_en',
      'region', 'region_en', 'id_country', 'id_office', 'updated_time',));
  foreach ($values as $record) {
    $query->values($record);
  }
  $query->execute();

  drupal_set_message(t('Updated !num records in Cities', array('!num' => count($values))));
}

function commerceecont_admin_update_cities_quarters($xml_array) {
  $xml = '<?xml version="1.0" ?>' . format_xml_elements($xml_array);
  //$xml_response = commerceecont_api_request($xml);
  $xml_response = file_get_contents('public://econt_quarters.xml');
  if (!$xml_response) return FALSE;
  $xml_element = new SimpleXMLElement($xml_response);
  if (!$xml_element->cities_quarters) return FALSE;
  $values = array();
  foreach ($xml_element->cities_quarters->e as $value) {
    $values[] = array(
        'id' => (string) $value->id,
        'id_city' => (string)$value->id_city,
        'name' => (string)$value->name,
        'name_en' => (string)$value->name_en,
        'updated_time' => strtotime((string)$value->updated_time),
    );
  }
  db_query('TRUNCATE commerceecont_quarters');
  $query = db_insert('commerceecont_quarters')->fields(array('id', 'id_city', 'name', 'name_en', 'updated_time'));
  foreach ($values as $record) {
    $query->values($record);
  }
  $query->execute();

  drupal_set_message(t('Updated !num records in Cities quarters', array('!num' => count($values))));
}*/


function commerceecont_test_request() {
  /*  $xml_array = [
        'requests' => [
            'client' => [
                'username' => 'demo',
                'password' => 'demo',
            ],
            'request_type' => $type,
        ]
    ];
    $xml = '<?xml version="1.0" ?>' . format_xml_elements($xml_array);


    $filename = '/tmp/se1.xml';
    file_put_contents($filename, $xml);

    $eecont_service_url = 'http://demo.econt.com/e-econt/xml_service_tool.php';

    $ch = curl_init($eecont_service_url);
    $args['file'] = curl_file_create($filename, 'text/xml', basename($filename));
    curl_setopt($ch, CURLOPT_URL, $eecont_service_url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    $info = curl_getinfo($ch);
    $curl_error = curl_error($ch);
    curl_close($ch);*/

}

function _commerceecont_delivery_callback($form, $form_state) {
  return $form['services']['swrapper'];
}


function commerceecont_import_address_database() {
  $types = commerceecont_admin_types_array();
  $schema = drupal_get_schema();
  foreach ($types as $type) {
    $file = drupal_get_path('module', 'commerceecont') . '/csv/' . $type['table_name'] . '.csv';
    if (file_exists($file)) {
      db_query('TRUNCATE ' . $type['table_name']);
      watchdog('commerceecont', 'Deafults: File @file has been loaded', array('@file' => $file));
      $query = db_insert($type['table_name'])->fields(array_keys($schema[$type['table_name']]['fields']));
      $i = 0;
      $handle = fopen($file, 'r');
      while ($row = fgetcsv($handle)) {
        $query->values($row);
        // Batch of 52k rows eats more than 1GB ram, Can't use batch here. Queue will need multiple crons
        if ($i % 1000 == 0)
          $query->execute();
        $i++;
      }
      $query->execute();
      watchdog('commerceecont', 'Deafults: Table @table has been updated', array('@table' => $type['table_name']));
    } else {
      watchdog('commerceecont', 'Deafults: File @file does not exists', array('@file' => $file), WATCHDOG_ERROR);
    }
  }
}