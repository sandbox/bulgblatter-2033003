<?php
/**
 * @file
 * Handles XML-related stuff for Commerce Econt module.
 */

/**
 * Generate XML
 *
 * @param $order
 */
function commerceecont_build_rate_request($order, $shipping_service) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  // Determine the shipping profile reference field name for the order.
  if (function_exists('commerce_physical_order_shipping_field_name')) {
    $field_name = commerce_physical_order_shipping_field_name($order);
    // Prepare the shipping address for use in the request.
    if (!empty($order_wrapper->{$field_name}->commerce_customer_address)) {
      $shipping_address = $order_wrapper->{$field_name}->commerce_customer_address->value();
    } else {
      $field = field_info_field($field_name);
      $instance = field_info_instance('commerce_customer_profile', $field_name, 'shipping');
      $shipping_address = addressfield_default_values($field, $instance);
    }
  } else {
    $shipping_address = $order_wrapper->commerce_customer_shipping->commerce_customer_address->value();
  }
//  dpm($order_wrapper->commerce_customer_shipping->value());
//  $shipping_profile = $order_wrapper->{$field_name}->value();


//  $shipping_service['tariff_sub_code'] = commerceecont_tariff_code($shipping_service, $shipping_address);
//  $shipping_service['tariff_code'] = commerceecont_tariff_code($shipping_service, $shipping_address);

//  if (!commerceecont_validate_address($shipping_address, $shipping_service)) {
//    drupal_set_message(t('%service could not determine shipping address! Please go back and verify
//    if address is correct.', array('%service' => $shipping_service['display_title'])));
//
//    return FALSE;
//  }
  if (function_exists('commerce_physical_order_weight')) {
    $weight = commerce_physical_order_weight($order, 'kg'); // this returns $weight['unit'] and $weight['weight']
  } else {
    $weight = array(
      'weight' => 0.5
    );
  }
  if (function_exists('commerce_physical_order_volume')) {
    $volume = commerce_physical_order_volume($order, 'cm'); // this returns $volume['unit'] and $weight['volume']
  } else {
    $volume = array(
      'volume' => 0
    );
  }

  /* If there is no total volume or weight for the order, there is no reason to send the request to UPS */
  if ($volume['volume'] == NULL || $weight['weight'] == NULL) {
    $weight['weight'] = 0.5;
    $volume['volume'] = 0;
  }

  $number_of_packages = 1;

  /* Pickup Schedule */

  /* Ship To - Customer Shipping Address */
  // Prepare the shipping address for use in the request.
  if (!empty($order_wrapper->commerce_customer_shipping->commerce_customer_address)) {
    $shipping_address = $order_wrapper->commerce_customer_shipping->commerce_customer_address->value();
  }
//  commerceecont_validate_shipping_address($shipping_address);
  $store_address = variable_get('commerceecont_store_address', array());
  if (empty($store_address['organisation_name'])) {
    $store_address['organisation_name'] = $store_address['name_line'];
  }
  commerceecont_tariff_codes($shipping_service, $shipping_address);


  $xml = new SimpleXMLElement('<parcels/>');

  $system = $xml->addChild('system');
  $system->addChild('validate', 0);
  $system->addChild('response_type', 'XML');
  $system->addChild('only_calculate', 1);

  $client = $xml->addChild('client');
  $client->addChild('username', variable_get('commerceecont_user', 'demo'));
  $client->addChild('password', variable_get('commerceecont_pass', 'demo'));

  $loadings = $xml->addChild('loadings');

  $row = $loadings->addChild('row');

  $sender = $row->addChild('sender');
  $sender->addChild('city', $store_address['city']);
  $sender->addChild('post_code', $store_address['post_code']);
  $sender_office = variable_get('commerceecont_deliver_from_office', '');
  if ($sender_office != '') {
    $sender->addChild('office_code', $sender_office);
  }
  $sender->addChild('name', $store_address['organisation_name']);
  $sender->addChild('name_person', $store_address['name_line']);
  $sender->addChild('email', $store_address['mail']);
  $sender->addChild('quarter', $store_address['quarter']);
  $sender->addChild('street', $store_address['street']);
  $sender->addChild('street_num', $store_address['street_num']);
  $sender->addChild('street_bl');
  $sender->addChild('street_vh');
  $sender->addChild('street_et');
  $sender->addChild('street_ap');
  $sender->addChild('street_other', $store_address['other']);
  $sender->addChild('phone_num', $store_address['phone']);
//  $sender->addChild('email_on_delivery', $store_address['mail']);

  $receiver = $row->addChild('receiver');
  $receiver->addChild('city', $shipping_address['locality']);
  $receiver->addChild('post_code', $shipping_address['postal_code']);
  $receiver->addChild('office_code');
  $receiver->addChild('name', $shipping_address['name_line']);
  $receiver->addChild('name_person', $shipping_address['name_line']);
  $receiver->addChild('email');
  $receiver->addChild('quarter');
  $receiver->addChild('street');
  $receiver->addChild('street_num');
  $receiver->addChild('street_bl');
  $receiver->addChild('street_vh');
  $receiver->addChild('street_et');
  $receiver->addChild('street_ap');
  $receiver->addChild('street_other');
  $receiver->addChild('phone_num', '12346789');
  $receiver->addChild('sms_no');

  $shipment = $row->addChild('shipment');
  $shipment->addChild('shipment_type', 'PACK');
  $shipment->addChild('description', 'A test request');
  $shipment->addChild('pack_count', $number_of_packages);
  $shipment->addChild('weight', $weight['weight']);
  $shipment->addChild('tariff_code', $shipping_service['tariff_code']);
  $shipment->addChild('tariff_sub_code', $shipping_service['tariff_sub_code']);

  $instruction_returns = $row->addChild('instruction_returns');
  $instruction_returns->addChild('shipping_returns');

  $payment = $row->addChild('payment');
  $payment->addChild('side', 'RECEIVER');
  $payment->addChild('method', 'CASH');

  $services = $row->addChild('services');
//  $services->addChild('p');
//  $services->addChild('type');
//  $services->addChild('e');
//  $services->addChild('e1');
//  $services->addChild('e2');
//  $services->addChild('e3');
//  $services->addChild('dc');
//  $services->addChild('dc_cp');
//  $services->addChild('dp');
//  $services->addChild('oc');
  $order_total = $order_wrapper->commerce_order_total->value();
  $cd = $services->addChild('cd', commerce_currency_amount_to_decimal($order_total['amount'], 'BGN'));
  $cd->addAttribute('type', 'GET');
//  $cd->addChild('');


  $xml = $xml->asXML();

  return $xml;
}

/**
 * Submit post request to
 *
 * @param $xml
 *  An XML string. Econt want to be submitted as a file
 *
 * @return bool|mixed
 *  Returns XML string or FALSE on error
 */
function commerceecont_api_request($vars) {
//  $temp_filename = drupal_tempnam('/tmp/', 'econt');
  $temp_filename = '/tmp/econt_' . md5(microtime()).'.xml';

  //@TODO: add that url to settings form
  if (variable_get('commerceecont_mode', 'demo') == 'demo') {
    $commerceecont_url = isset($vars['request']) && $vars['request'] == 'service' ? EECONT_DEMO_SERVICE_URL : EECONT_DEMO_IMPORT_URL;
  } else {
    $commerceecont_url = isset($vars['request']) && $vars['request'] == 'service' ? EECONT_SERVICE_URL : EECONT_IMPORT_URL;
  }
  if (file_put_contents($temp_filename, $vars['xml']) === FALSE) {
    drupal_set_message(t('Unable to generate request data.'), 'error');
    watchdog('econt', '@url, @message:<pre>@xml</pre>', array('@message' => $vars['message'], '@xml' => $vars['xml'], '@url' => $commerceecont_url));
    return FALSE;
  }
  if (variable_get('commerceecont_log_request', FALSE)) {
    watchdog('econt', 'API request initiated: @url, @message:<pre>@xml</pre>', array('@message' => $vars['message'], '@xml' => $vars['xml'], '@url' => $commerceecont_url));
  }
  $ch = curl_init();
  if (class_exists('CURLFile')) {
    $args['file'] = new CurlFile($temp_filename, 'application/xml', basename($temp_filename));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
  } else {
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('file' => "@$temp_filename"));
  }

  curl_setopt($ch, CURLOPT_URL, $commerceecont_url);
  curl_setopt($ch, CURLOPT_TIMEOUT, 30);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

  $xml_response = curl_exec($ch);
  $curl_error = curl_error($ch);
  $info = curl_getinfo($ch);
  curl_close($ch);
//  unlink($temp_filename);

  if ($curl_error) {
    watchdog('econt', 'cURL error: @error', array('@error' => $curl_error), WATCHDOG_ERROR);

    return FALSE;
  }
  if (!empty($xml_response)) {
    if (variable_get('commerceecont_save_response', FALSE)) {
      file_put_contents('public://econt/shipping/xml_responce_' . microtime() . '.xml', $xml_response);
    }
    $response = new SimpleXMLElement($xml_response);
    // Log the API request if specified.
    if (variable_get('commerceecont_log_response', FALSE)) {
      watchdog('econt', 'API response received: @time <pre>@xml</pre>', array('@xml' => $response->asXML(), '@time' => microtime()));
    }
    return $response;
  }

  return FALSE;
}

function commerceecont_fix_postal_code($shipping_address) {
  $query = db_select('commerceecont_offices', 'co');
  $query->join('commerceecont_cities', 'cc', 'co.id_city = cc.id');
  $query->join('commerceecont_regions', 'cr', 'cr.id_city = cc.id');
  $query->fields('cc', array('post_code'))
    ->condition('cr.code', $shipping_address['postal_code']);
  $result = $query->execute()->fetchField();
  if (!$result) {
    $query = db_select('commerceecont_offices', 'co');
    $query->join('commerceecont_cities', 'cc', 'co.id_city = cc.id');
    $query->join('commerceecont_regions', 'cr', 'cr.id_city = cc.id');
    $query->fields('cc', array('post_code'))
        ->condition('cr.name', $shipping_address['locality']);
    $result = $query->execute()->fetchField();
  }
  return $result;
}